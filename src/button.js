import React from 'react';
import {clik} from './robots.js'
import {deleteRobo} from './robots.js'
import './style.css'
const buttons=(props)=>{
    return(
        <div className = 'mb-3 button  '>
            <button onClick={clik} className='btn btn-primary add mr-3 '>ADD ROBOTS</button>
            <button onClick={deleteRobo} className='btn btn-danger mr-3'>DELETE ROBOTS</button>
            <button className='btn btn-primary' onClick = {props.onDisplayClick}>DISPLAY ROBOTS</button>
        </div>
        
    );
}

export default buttons;