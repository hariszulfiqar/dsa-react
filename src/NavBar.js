import React from 'react'
import DropDown from './dropdown'
import './style.css'
const NavBar=(props)=>{
    return(
        <div>
            <div >
                <DropDown tag='Add Friends' id = {props.id} task = "add"/>
                <br/>
                <DropDown tag='Delete Friends' id = {props.id} task = 'delete'/>
            </div>
            <div>
                <button className='btn-info' onClick = {() => props.onShowFriends()}>Show Friends</button>
                <button className='btn-success'>Chat Box</button>
            </div>
            
        </div>
        )
}
export default NavBar;
