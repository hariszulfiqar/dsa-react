import React, { Component } from "react";
import "./dropdown.css";
import { robos, setRobots } from "./robots.js";
import Select from "react-select";

class Dropdown extends Component {
  constructor(props) {
    super(props);
	}
	
	state = {
		selectedFraandsssxxxx: []
	}

  dropDownOptions = () => {
    let robots = robos;
    let options = [];
    robots.map(item => {
      options.push({
        value: item.id,
        label: item.name
      });
    });
    return options;
  };

	handleSubmission = () => {
		let frandxxxx = this.state.selectedFraandsssxxxx
		let currentRobot = this.props.id
    let robots = robos
    let currentRobotObject =  robots.find(item => item.id === currentRobot)
    if(currentRobotObject){
      let currentRobotFriends = currentRobotObject.friends
      frandxxxx.forEach(item => {
        if(!currentRobotFriends.includes(item.value)){
          currentRobotFriends.push(item.value)
        }
      })
      frandxxxx.forEach(selected => {
        robots.forEach(item => {
          if(item.id === selected.value){
            let friends = item.friends
            if(!friends.includes(currentRobot)){
              friends.push(currentRobot)
            }
          }
        })
      })
    }
		setRobots(robots)
  }
  
  handleRemovalSubmission = () => {
    let frandxxxx = this.state.selectedFraandsssxxxx
    let currentRobot = this.props.id
    let robots = robos
    let currentRobotObject = robots.find(item => item.id === currentRobot)
    if(currentRobotObject){
      let currentRobotFriends = currentRobotObject.friends
      frandxxxx.forEach((item, key) => {
        if(currentRobotFriends.includes(item.value)){
          let index = currentRobotFriends.findIndex(currentItem => currentItem === item.value)
          if(index !== -1){
            currentRobotFriends.splice(index,1)
          }
        }
      })

      frandxxxx.forEach(selected => {
        robots.forEach(item => {
          if(item.id === selected.value){
            let friends = item.friends
            if(friends.includes(currentRobot)){
              let index = friends.findIndex(currentFriend => currentFriend === currentRobot)
              if(index !== -1){
                friends.splice(index, 1)
              }
            }
          }
        })
      })
    }
  }


  render() {
    const customStyles = {
      control: () => ({
        width: "100%",
      })
    }
    
    return (
      <div className="dropdown" style = {{ width: '100%' }}>
        <button className="dropbtn">{this.props.tag}</button>
        <div className="dropdown-content" style = {{ width: '100%' }}>
          <Select
            styles = {customStyles}
            className='area'
            isMulti
            options={this.dropDownOptions()}
            onChange={value => {
							this.setState({ 
								selectedFraandsssxxxx: value
							})
						}}
          />
          {
            this.props.task === 'add' ?
              <button className="btn-info submit" style = {{ width: '100%' }} onClick = {() => this.handleSubmission()}>Submit Add Friends</button>
              :
              <button className="btn-info submit" style = {{ width: '100%' }} onClick = {() => this.handleRemovalSubmission()}>Submit Remove Friends</button>              
          }
        </div>
      </div>
    );
  }
}

export default Dropdown;

const addFriends = data => {
  console.log(data);
};
