import React, { Component } from "react";
import Buttons from "./button.js";
import { robos } from "./robots.js";
import "./style.css";
import Navbar from "./NavBar.js";
import Dropdown from "./dropdown.js";
class App extends Component {
  state = {
    displayRobots: false,
    showFriends: false
  };
  render() {
    return (
      <div>
        <h1 className="header f1 text-center">ROBO FRIENDS</h1>
        <Buttons
          onDisplayClick={() => {
            if (robos.length != 0)
              this.setState({ displayRobots: true, robos: robos });
          }}
        />
        {this.state.displayRobots ? (
          this.state.robos.map((item, key) => {
            return (
              <div className="bg-light-blue dib br3 pa3 ma2 profile " key={key}>
                <Navbar
                  id = {item.id}
                  onShowFriends={() =>
                    this.setState({ showFriends: !this.state.showFriends })
                  }
                />
                {!this.state.showFriends ? (
                  <React.Fragment>
                    <img className="rounded mx-auto d-block" src={item.image} />
                    <h1 className="text-center">Name : {item.name}</h1>
                    <p className="text-center">Email : {item.email}</p>
                  </React.Fragment>
                ) : (
                    <div>
                      <br/>
                      {
                        item.friends && item.friends.length > 0 ?  
                        item.friends.map(item => {
                            let currentRobot = robos.find(rob => rob.id === item )
                            if(currentRobot){
                              return <div>►  {currentRobot.name} </div>
                            }
                        })
                        :
                        <span>This robot is a nerd. No Social Life.</span>
                      }
                    </div>
                )}
              </div>
            );
          })
        ) : (
          <div className="someText f2 text-center">
            NO ROBOTS EXISTS PLEASE ADD ROBOTS
          </div>
        )}
      </div>
    );
  }
}

export default App;

// const index = this.state.robos.findIndex(item => (item.id === personalID)) // Returns index if condition satisfied
// const object = this.state.robos.find(item => (item.id === personalID)) // Return object if specific condition is satisfied
// this.state.robos.splice(index, 1) // Deletes the element found at the specific index
// ['1', null, '3'].includes("3") // Return as boolean if value is found
